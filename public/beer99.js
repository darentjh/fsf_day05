/**
 * Created by Daren on 1/4/16.
 */

var i = 0;
var j = 0;

for(i=99; i>0; i--) {
    var line0 = i + " bottles of beer on the wall, " + i + " bottles of beer. <br>";
    var line1;

    // for(j = 0; j < i; j++); THIS IS ANOTHER ALTERNATIVE
    // for(j = 99; j > 0; j--); THIS IS WRONG
    for(j=i; j>0; j--) {
        document.write("<img src = 'images/beer-img.png'>")
    }

    switch (i) {
        case 2: // i==2
            line1 = "Take one down and pass it around, " +  " one bottle of beer on the wall. <br><br>";
            break;

        case 1: // i==1
            line0 = "1 bottle of beer on the wall, " +  "1 bottle of beer. <br>";
            line1 = "Take one down and pass it around, no more bottles of beer on the wall. <br><br>";
            break;

        default: //otherwise
            line1 = "Take one down and pass it around, " + (i-1) + " bottles of beer on the wall. <br><br>"
            break;

    }

    // if (i==2) {
    //     line1 = "Take one down and pass it around, " + (i-1) + " bottle of beer on the wall. <br><br>";
    // } else if (i==1) {
    //     line0 = i + " bottle of beer on the wall, " + i + " bottle of beer. <br>";
    //     line1 = "Take one down and pass it around, " + "no more" + " bottles of beer on the wall. <br><br>";
    // } else {
    //     line1 = "Take one down and pass it around, " + (i-1) + " bottles of beer on the wall. <br><br>"
    // }

    document.write("<p>");
    document.write(line0);
    document.write(line1);
}

document.write("No more bottles of beer on the wall, no more bottles of beer.<br/>"
+ "Go to the store to buy some more. 99 bottles of beer on the wall.")