/**
 * Created by Daren on 1/4/16.
 */
//load express
var express = require("express");

//create an express app
var app = express();

//specify doc root - static files
app.use(
    express.static(__dirname + "/public")
);

//listen to port 3000
app.listen(3000, function(){
    console.info("Application has started on port 3000");
});